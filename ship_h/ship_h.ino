/*
  Ship.h - Library for the Ship
  Created by J.G., March 30, 2014.
  Released into the public domain.
*/
#ifndef Ship_h
#define Ship_h

#include "Arduino.h"

class Ship{
  public:
    Ship();
    void move();
    void shoot();
  private:
    int ship_x;
    int ship_y;
    int shot_x;
    int shot_y;
    int ship_x_delay;
    int ship_y_delay;
    float ship_x_vel;
    float ship_y_vel;
    
    Ship::Ship(){
      this.ship_y = 40;
    }
    
};

#endif
